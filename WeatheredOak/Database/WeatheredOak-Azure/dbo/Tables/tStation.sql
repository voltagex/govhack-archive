﻿CREATE TABLE [dbo].[tStation] (
    [StationID]   VARCHAR (50) NOT NULL,
    [StationName] VARCHAR (50) NOT NULL,
    [Latitude]    FLOAT (53)   NOT NULL,
    [Longitude]   FLOAT (53)   NOT NULL,
    [Elevation]   INT          NOT NULL,
    [StartYear]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_tStation] PRIMARY KEY CLUSTERED ([StationID] ASC)
);

