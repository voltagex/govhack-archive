﻿CREATE TABLE [dbo].[tStationBatch] (
    [StationBatchID] INT           IDENTITY (1, 1) NOT NULL,
    [TempMaxUrl]     VARCHAR (500) NULL,
    [TempMinUrl]     VARCHAR (500) NULL,
    [StationID]      VARCHAR (50)  NULL,
    [LastUpdated]    DATETIME      NULL,
    [HttpDate]       DATETIME      NULL,
    CONSTRAINT [PK_tStationBatch] PRIMARY KEY CLUSTERED ([StationBatchID] ASC)
);

