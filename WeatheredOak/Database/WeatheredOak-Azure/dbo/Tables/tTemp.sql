﻿CREATE TABLE [dbo].[tTemp] (
    [Date]       VARCHAR (50)  NOT NULL,
    [MinTemp]    FLOAT (53)    NOT NULL,
    [MaxTemp]    FLOAT (53)    NOT NULL,
    [TempID]     INT           IDENTITY (1, 1) NOT NULL,
    [StationID]  VARCHAR (50)  NOT NULL,
    [ActualDate] DATETIME2 (7) NULL,
    [Rainfall]   FLOAT (53)    NULL,
    CONSTRAINT [PK_tTemp] PRIMARY KEY CLUSTERED ([TempID] ASC)
);

