﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE Procedure [dbo].[spGetTempsByDateRangeAndStation]
@StartDate VARCHAR(50),
@EndDate VARCHAR(50),
@StationID VARCHAR(50)

AS BEGIN

SELECT [Date],MinTemp,MaxTemp,Rainfall,TempID,StationID
  FROM [AcornData].[dbo].[tTemp] WHERE StationID = @StationID AND (ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate))
  
  END