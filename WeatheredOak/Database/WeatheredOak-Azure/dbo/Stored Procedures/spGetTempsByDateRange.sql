﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE Procedure [dbo].[spGetTempsByDateRange]
@StartDate VARCHAR(50),
@EndDate VARCHAR(50)

AS BEGIN

SELECT [Date],MinTemp,MaxTemp,Rainfall,TempID,StationID
  FROM [AcornData].[dbo].[tTemp] WHERE ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate)
  
  END