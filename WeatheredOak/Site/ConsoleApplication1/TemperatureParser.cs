﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornData.Entities;

namespace ConsoleApplication1
{
    public class TemperatureParser
    {
        //ACORN-SAT_MIN_TEMP  023090 19100101 20111231 missing_value=99999.9 Adelaide
        //19100101    20.8

        public static List<tTemp> CreateTemperatureObjects(string minTemps, string maxTemps, string stationID)
        {
            List<tTemp> maxObject = new List<tTemp>();
            List<tTemp> minObject = new List<tTemp>();
            var comaparer = new GenericEqualityComparer<tTemp>(o => o.Date);

            foreach (string temperature in (minTemps.Split(new string[] {"\r\n"},StringSplitOptions.None).Skip(1))) //probably shouldn't skip the first
            {
                if (temperature.Length == 0)
                    break;

                string date = temperature.Substring(0, 8);
                string temp = temperature.Substring(8).Trim();
                minObject.Add(new tTemp() { Date = date, StationID = stationID, MinTemp = double.Parse(temp) });
            }

            foreach (string temperature in (maxTemps.Split(new string[] { "\r\n" }, StringSplitOptions.None).Skip(1))) //probably shouldn't skip the first
            {
                if (temperature.Length == 0)
                    break;
                string date = temperature.Substring(0, 8);
                string temp = temperature.Substring(8).Trim();
                maxObject.Add(new tTemp() { Date = date, StationID = stationID, MaxTemp = double.Parse(temp) });
            }
            List<tTemp> relatedTemps = new List<tTemp>();

            for(int i = 0; i < minObject.Count; i++)
            {
                tTemp te = minObject[i];
                for (int j = 0; j < maxObject.Count; j++)
                {
                    if (maxObject[j].Date == minObject[i].Date)
                    {
                        minObject[i].MaxTemp = maxObject[j].MaxTemp;
                        break;
                    }
                }
                //te.MaxTemp = maxObject.Where(x =>
                //te.Date == x.Date
                //).First().MaxTemp;

            }
            //for (int i = 0; i < minObject.Count; i++)
            //{
            //    if (maxObject[i].Date == minObject[i].Date)
            //    {
            //        maxObject[i].MinTemp = minObject[i].MinTemp;
            //        relatedTemps.Add(maxObject[i]);
            //    }
            //    else
            //        throw new Exception();
            //}

            return minObject;

        }



    }


    /// <summary>
    /// http://stackoverflow.com/questions/5909259/generic-iequalitycomparert-and-gethashcode
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericEqualityComparer<T> : IEqualityComparer<T>
    {
        Func<T, T, bool> compareFunction;
        Func<T, int> hashFunction;

        public GenericEqualityComparer(Func<T, object> projection)
        {
            compareFunction = (t1, t2) => projection(t1).Equals(projection(t2));
            hashFunction = t => projection(t).GetHashCode();
        }

        public GenericEqualityComparer(Func<T, T, bool> compareFunction, Func<T, int> hashFunction)
        {
            this.compareFunction = compareFunction;
            this.hashFunction = hashFunction;
        }

        public bool Equals(T x, T y)
        {
            return compareFunction(x, y);
        }

        public int GetHashCode(T obj)
        {
            return hashFunction(obj);
        }
    }   
}
