﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using AcornData.Entities;
using System.Configuration;
using ServiceStack.Text;
using AcornData.Entities.NonDatabaseEntities;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            LoadRainfallCSVs();
        }

        private static void LoadRainfallCSVs()
        {
            DirectoryInfo dir = new DirectoryInfo(@"C:\Data\rainfall\");
            List<FileInfo> files = new List<FileInfo>(dir.EnumerateFiles("*.csv"));
                                
                foreach (FileInfo file in files)
                {
                    using (StreamReader reader = file.OpenText())
                    {
                        List<RainfallSpecific> rainfall = RainfallParser.GetRainfallForStationAndDate(reader.ReadToEnd());
                        using (AcornDataEntities entities = new AcornDataEntities())
                        {
              
                  
                        string stationId = rainfall[0].StationID;
                          var allTemperatures = (from temp in entities.tTemps
                                                 where temp.StationID == stationId
                                                select temp).ToList();

                        foreach (RainfallSpecific item in rainfall)
                        {
                            var current = from otherItem in allTemperatures
                                          where otherItem.Date == item.Date
                                          select otherItem;

                            if (current.Count() > 0)
                            {
                                var validTemp = current.First();
                                if (validTemp == null)
                                    throw new Exception();
                                validTemp.Rainfall = item.RainfallAmount;
                            }
                        }

                        Console.WriteLine("Saving " + stationId);
                        entities.SaveChanges();
                    }
                    Console.WriteLine("Finished " + file.Name);
                }
                
            }

        }

        private static void BeginRainfallScrape()
        {

            string allStationsURL = "http://govhack.typ0.co/api/stations/all?format=json";
            List<tStation> allStations = new List<tStation>();
            JsonSerializer<List<tStation>> jsonSerializer = new JsonSerializer<List<tStation>>();
            string stationString = CookieAwareWebClient.SingletonWebClient.DownloadString(allStationsURL);

            allStations = jsonSerializer.DeserializeFromString(stationString);

            string bomURL = "http://www.bom.gov.au";
            string baseURL = "http://www.bom.gov.au/jsp/ncc/cdio/weatherData/av?p_nccObsCode=136&p_display_type=dailyDataFile&p_stn_num=";
            string mainPage = string.Empty;
            HtmlDocument doc = new HtmlDocument();
            foreach (tStation station in allStations)
            {
                mainPage = CookieAwareWebClient.SingletonWebClient.DownloadString(baseURL + station.StationID);
                doc.LoadHtml(mainPage);
                string downloadURL = doc.DocumentNode.Descendants("a").Where(x => x.Attributes["title"] != null && x.Attributes["title"].Value == "Data file for daily rainfall data for all years").First().Attributes["href"].Value;
                downloadURL = downloadURL.Replace("&amp;", "&");
                downloadURL = bomURL + downloadURL;
                CookieAwareWebClient.SingletonWebClient.DownloadFile(downloadURL, station.StationID.ToString() + ".zip");
                Console.WriteLine(station.StationID);
            }
        }

        private static void BeginTemperatureScrape()
        {
            List<tStation> listOStation = new List<tStation>();
            List<tStationBatch> listOfStationBatch = new List<tStationBatch>();

            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.OptionFixNestedTags = false;

            string baseUrl = "http://www.bom.gov.au/";
            string startPoint = "climate/change/acorn-sat";
            MemoryStream mem = new MemoryStream(
                CookieAwareWebClient.SingletonWebClient.DownloadData(new Uri(baseUrl + startPoint)));

            htmlDoc.Load(mem);

            var dataTab = (from data in htmlDoc.DocumentNode.Descendants("table")
                           where data.Attributes != null &&
                           data.Attributes["id"] != null &&
                           data.Attributes["id"].Value == "acorn-sat-table"
                           select data).First();

            var tableOfStuff = from stuff in dataTab.Descendants("tbody").First().Descendants("tr")
                               select stuff;

            foreach (HtmlNode htmlNode in tableOfStuff)
            {
                tStationBatch stationBatch = new tStationBatch();
                tStation station = new tStation();
                var tds = from td in htmlNode.Descendants("td")
                          select td;

                var lTds = tds.ToList();

                station.StationID = lTds[0].InnerText;
                station.StationName = lTds[1].InnerText;

                //number 0
                //name 1
                station.Latitude = double.Parse(lTds[2].InnerText);//lat 2
                station.Longitude = double.Parse(lTds[3].InnerText);//long 3
                station.Elevation = int.Parse(lTds[4].InnerText); //elevation 4
                station.StartYear = lTds[5].InnerText.Trim();
                //minURL 6
                //maxURL 7
                stationBatch.StationID = station.StationID;
                stationBatch.TempMinUrl = baseUrl + lTds[6].Descendants("a").First().Attributes["href"].Value.Substring(1);
                stationBatch.TempMaxUrl = baseUrl + lTds[7].Descendants("a").First().Attributes["href"].Value.Substring(1);
                listOfStationBatch.Add(stationBatch);

                SaveStationToDb(station);

            }

            //GetTempsForStationBatch(listOfStationBatch);
        }

        private static void GetTempsForStationBatch(List<tStationBatch> listOfStationBatch)
        {
            foreach (tStationBatch stationB in listOfStationBatch)
            {
                List<tTemp> finalTemps = new List<tTemp>();
                byte[] minTempsBytes = CookieAwareWebClient.SingletonWebClient.DownloadData(stationB.TempMinUrl);
                byte[] maxTempsBytes = CookieAwareWebClient.SingletonWebClient.DownloadData(stationB.TempMaxUrl);
                string sMintemps = UTF8Encoding.UTF8.GetString(minTempsBytes);
                string sMaxtemps = UTF8Encoding.UTF8.GetString(maxTempsBytes);

                finalTemps = TemperatureParser.CreateTemperatureObjects(sMintemps, sMaxtemps, stationB.StationID);
                SaveTempToDb(finalTemps);

                Console.WriteLine("Time: " + DateTime.Now + " - Station: " + stationB.StationID);
            }
        }

        private static void SaveTempToDb(List<tTemp> finalTemps)
        {
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                foreach (tTemp temp in finalTemps)
                {
                    entities.tTemps.AddObject(temp);
                }

                entities.SaveChanges();
            }
        }

        private static void SaveStationToDb(tStation station)
        {
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                entities.tStations.AddObject(station);

                entities.SaveChanges();
            }
        }
    }
}
