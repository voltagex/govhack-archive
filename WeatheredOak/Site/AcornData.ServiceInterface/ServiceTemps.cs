﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AcornData.Entities;
using ServiceStack.ServiceInterface;
using ServiceStack.Common.Web;
using System.Globalization;
using ServiceStack.ServiceHost;
namespace AcornData.ServiceInterface
{
    public class ServiceTemps : RestServiceBase<tTempRequest>
    {
        public override object OnGet(tTempRequest request)
        {
            if (RequestContext.AbsoluteUri.Contains("/api/temps/daterange/"))
            {
                try
                {
                    return RequestContext.ToOptimizedResult(GetTempsBetweenRange(request));
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else if (RequestContext.AbsoluteUri.Contains("/api/temps/station/id/") && string.IsNullOrEmpty(request.eDate) && string.IsNullOrEmpty(request.sDate))
            {
                return RequestContext.ToOptimizedResult(GetAllForStationID(request));
            }
            else if (RequestContext.AbsoluteUri.Contains("/api/temps/station/id/") && !string.IsNullOrEmpty(request.eDate) && !string.IsNullOrEmpty(request.sDate))
            {
                return RequestContext.ToOptimizedResult(GetTempsBetweenRangeByStation(request));
            }
            else
            {
                //This shouldn't be hit
                throw HttpError.NotFound("Path not found");
            }
        }

        private List<tTemp> GetTempsBetweenRangeByStation(tTempRequest request)
        {
            List<tTemp> result = new List<tTemp>();
            string strStartDate = request.sDate.Trim();
            string strEndDate = request.eDate.Trim();

            using (AcornDataEntities entities = new AcornDataEntities())
            {

                var temps = from temp in entities.spGetTempsByDateRangeAndStation(strStartDate, strEndDate,request.StationID)
                            select temp;

                result = temps.ToList();
            }

            return result;
        }

        private List<tTemp> GetTempsBetweenRange(tTempRequest request)
        {
            List<tTemp> result = new List<tTemp>();
            string strStartDate = request.sDate.Trim();
            string strEndDate = request.eDate.Trim();

            using (AcornDataEntities entities = new AcornDataEntities())
            {

                var temps = from temp in entities.spGetTempsByDateRange(strStartDate, strEndDate)
                            select temp;

                result = temps.ToList();
            }

            return result;
        }

        private List<tTemp> GetAllForStationID(tTempRequest request)
        {
            List<tTemp> result = new List<tTemp>();
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var temps = from temp in entities.tTemps
                            where temp.StationID == request.StationID
                            select temp;

                result = temps.ToList();
            }

            return result;
        }
    }
}
