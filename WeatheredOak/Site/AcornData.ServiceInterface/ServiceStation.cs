﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.ServiceInterface;
using AcornData.Entities;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
namespace AcornData.ServiceInterface
{
    public class ServiceStation : RestServiceBase<tStation>
    {
        public override object OnGet(tStation request)
        {
            if (RequestContext.AbsoluteUri.Contains("/api/station/id/"))
            {
                return RequestContext.ToOptimizedResult(GetStationById(request));
            }
            else if (RequestContext.AbsoluteUri.Contains("/api/stations/name"))
            {
                return RequestContext.ToOptimizedResult(GetStationsByName(request));
            }
            else if (RequestContext.AbsoluteUri.Contains("/api/station/geo") && request.Latitude != null && request.Longitude != null)
            {
                throw HttpError.NotFound("Path not found");
            }
            else if (RequestContext.AbsoluteUri.Contains("/api/stations/all"))
            {
                return RequestContext.ToOptimizedResult(GetAllStation(request));
            }
            else
            {

                throw HttpError.NotFound("Path not found");
            }
        }

        /// <summary>
        /// Ran out of time
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private List<tStation> GetStationByGeoLocation(tStation request)
        {
            List<tStation> result = new List<tStation>();
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var stations = from station in entities.tStations
                               where station.StationID == request.StationID
                               select station;

                if (stations.Count() == 1)
                    result = stations.ToList();
            }

            return result;
        }

        private List<tStation> GetStationsByName(tStation request)
        {
            List<tStation> result = new List<tStation>();
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var stations = from station in entities.tStations
                               where station.StationName.StartsWith(request.StationName)
                               orderby station.StationName ascending
                               select station;

                result = stations.ToList();
            }

            return result;
        }

        private List<tStation> GetAllStation(tStation request)
        {
            List<tStation> result = new List<tStation>();
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var stations = from station in entities.tStations
                               orderby station.StationName ascending
                               select station;

                result = stations.ToList();
            }

            return result;
        }

        private tStation GetStationById(tStation request)
        {
            tStation result = new tStation();
            using (AcornDataEntities entities = new AcornDataEntities())
            {
                var stations = from station in entities.tStations
                               where station.StationID == request.StationID
                               select station;

                if (stations.Count() == 1)
                    result = stations.First();
            }

            return result;
        }
    }
}
