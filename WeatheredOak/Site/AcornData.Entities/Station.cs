﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace AcornData.Entities
{
    [RestService("/api/station/id/{StationID}")]
    [RestService("/api/stations/name/{StationName}")]
    [RestService("/api/station/geo/{Longitude}/{Latitude}")]
    [RestService("/api/stations/all")]
    public partial class tStation
    {
        public System.Data.EntityKey EntityKey { get; private set; }
    }

    //[RestService("/api/temp/station/id/{StationId}/first")]
    //[RestService("/api/temp/station/id/{StationId}/dates/{StartDate}/{EndDate}")]
    //[RestService("/api/temps/station/id/{StationID}")]
    public partial class tTemp
    {
        public System.Data.EntityKey EntityKey { get; private set; }

    }

    [RestService("/api/temps/daterange/{sDate}/{eDate}")]
    [RestService("/api/temps/station/id/{StationID}/daterange/{sDate}/{eDate}")]
    [RestService("/api/temps/station/id/{StationID}")]
    [RestService("/api/temps/all")]
    public class tTempRequest
    {
        public List<tTemp> Temps { get; set; }
        public string sDate { get; set; }
        public string eDate { get; set; }
        public string StationID { get; set; }
    }

    public class tTempResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }




    public class tAverageTempResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }

    [RestService("/api/average/{RequestType}/station/id/{StationID}/daterange/{sDate}/{eDate}")]
    public class tAverageRequest
    {
        public string RequestType { get; set; }
        public string sDate { get; set; }
        public string eDate { get; set; }
        public string StationID { get; set; }
    }

    public class tAverageResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
