USE [AcornData]
GO
/****** Object:  StoredProcedure [dbo].[spGetAverageRainfallByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
DROP PROCEDURE [dbo].[spGetAverageRainfallByDateRange]
GO
/****** Object:  StoredProcedure [dbo].[spGetAverageTempsByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
DROP PROCEDURE [dbo].[spGetAverageTempsByDateRange]
GO
/****** Object:  StoredProcedure [dbo].[spGetTempsByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
DROP PROCEDURE [dbo].[spGetTempsByDateRange]
GO
/****** Object:  StoredProcedure [dbo].[spGetTempsByDateRangeAndStation]    Script Date: 07/03/2012 16:46:27 ******/
DROP PROCEDURE [dbo].[spGetTempsByDateRangeAndStation]
GO
/****** Object:  StoredProcedure [dbo].[spGetTempsByDateRangeAndStation]    Script Date: 07/03/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetTempsByDateRangeAndStation]
@StartDate VARCHAR(50),
@EndDate VARCHAR(50),
@StationID VARCHAR(50)

AS BEGIN

SELECT [Date],MinTemp,MaxTemp,Rainfall,TempID,StationID
  FROM [AcornData].[dbo].[tTemp] WHERE StationID = @StationID AND (ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate))
  
  END
GO
/****** Object:  StoredProcedure [dbo].[spGetTempsByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[spGetTempsByDateRange]
@StartDate VARCHAR(50),
@EndDate VARCHAR(50)

AS BEGIN

SELECT [Date],MinTemp,MaxTemp,Rainfall,TempID,StationID
  FROM [AcornData].[dbo].[tTemp] WHERE ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate)
  
  END
GO
/****** Object:  StoredProcedure [dbo].[spGetAverageTempsByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--This is a hack, because Entity Framework doesn't support multiple resultsets until EF5.
--There also may be a way to do averages in one line. Patches anyone?
CREATE PROCEDURE [dbo].[spGetAverageTempsByDateRange]
@StationID VARCHAR(50),
@StartDate VARCHAR(50),
@EndDate   VARCHAR(50)
AS 
BEGIN
DECLARE @MinTemp float,
@MaxTemp float

	SET @MinTemp = (SELECT AVG(MinTemp) from tTemp 
					WHERE (ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate)) AND StationID = @StationID AND MinTemp != 99999.9)
	SET @MaxTemp = (SELECT AVG(MaxTemp)	from tTemp 
					WHERE (ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate)) AND StationID = @StationID AND MaxTemp != 99999.9)
					
    SELECT @MinTemp as MinTemp, @MaxTemp as MaxTemp
END
GO
/****** Object:  StoredProcedure [dbo].[spGetAverageRainfallByDateRange]    Script Date: 07/03/2012 16:46:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spGetAverageRainfallByDateRange]
@StationID VARCHAR(50),
@StartDate VARCHAR(50),
@EndDate   VARCHAR(50)
AS 
BEGIN
	SELECT AVG(Rainfall) as Rainfall
		from tTemp WHERE ActualDate >= Convert(DateTime,@StartDate) AND ActualDate <= Convert(DateTime,@EndDate) AND StationID = @StationID AND Rainfall IS NOT NULL 
END
GO
