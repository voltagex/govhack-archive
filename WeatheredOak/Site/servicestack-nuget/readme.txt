This is a very temporary fix for the CSV output issue - there's a fix in mainline ServiceStack but NuGet doesn't have it yet.

I've bumped ServiceStack.Text to 3.8.4 - this version doesn't exist yet.
When it does, remove my ServiceStack.Text and the servicestack-nuget folder I've created, then re-get ServiceStack.Text from the official package source.